import React from 'react';
import { Hero } from './Hero.jsx';

const HEROES = [
    {
        id: 1,
        name: 'Darth Vader',
        health: '500',
        image: 'https://media.giphy.com/media/xT9DPlL2P7TL2HjxPa/giphy.gif'
    }, {
        id: 2,
        name: 'Princess Leia',
        health: '500',
        image: 'https://media.giphy.com/media/a5auAyjyCOf1S/giphy.gif'
    }, {
        id: 3,
        name: 'Luke Skywalker',
        healt: '500',
        image: 'https://media.giphy.com/media/Qx90DIQmcJlte/giphy.gif'
    }, {
        id: 4,
        name: 'Chewbacca',
        health: '500',
        image: 'https://media.giphy.com/media/RUUdVZqwpfTRS/giphy.gif'
    }, {
        id: 5,
        name: 'Darth Vader',
        health: '500',
        image: 'https://media.giphy.com/media/xT9DPlL2P7TL2HjxPa/giphy.gif'
    }, {
        id: 6,
        name: 'Princess Leia',
        health: '500',
        image: 'https://media.giphy.com/media/a5auAyjyCOf1S/giphy.gif'
    }
];

export class HeroesList extends React.Component {
    render() {
        return (
            <div>
                <div id="user">
                    <div className="heroes">
                        <ul className="heroes-list">
                            {
                                HEROES.map(function (el) {
                                    return <Hero
                                        key={el.id}
                                        name={el.name}
                                        health={el.health}
                                        image={el.image}
                                    />;
                                })
                            }
                        </ul>
                    </div>
                </div>
                <div id="opponent">
                    <div className="heroes">
                        <ul className="heroes-list">
                            {
                                HEROES.map(function (el) {
                                    return <Hero
                                        key={el.id}
                                        name={el.name}
                                        health={el.health}
                                        image={el.image}
                                    />;
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}