const webpack = require('webpack');
const ManifestPlugin = require('webpack-manifest-plugin');
const path = require('path');

module.exports = {
    entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, "public/build"),
        publicPath: "public/",
        filename: "bundle.js",
    },
    plugins: [
        new ManifestPlugin({
            path: path.resolve(__dirname,  'public'),
            fileName: "manifest.json"
        })
    ],
    "module": {
        rules: [
            {
                "exclude": "/node_modules/",
                "loader": "babel-loader",
                "query": {
                    "presets": ["es2015", "react"]
                },
                "test": /\.jsx?$/
            },
            {
                "test": /\.css$/,
                "exclude": /node_modules/,
                "loader": ['style-loader', 'css-loader'],
            },
            {
                test: /\.svg$/,
                loader: 'svg-inline-loader'
            }
        ]
    },
    devServer: {
        contentBase: path.join(__dirname, "public"),
        compress: true,
        port: 9000
    }
};