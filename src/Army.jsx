import React from 'react';
import { ArmyHero } from './ArmyHero.jsx';

const HEROES = [
    {
        id: 1,
        name: 'Darth Vader',
        health: '1234',
        damage: '1234',
        image: 'https://media.giphy.com/media/xT9DPlL2P7TL2HjxPa/giphy.gif'
    }, {
        id: 2,
        name: 'Princess Leia',
        health: '1234',
        damage: '1234',
        image: 'https://media.giphy.com/media/a5auAyjyCOf1S/giphy.gif'
    }, {
        id: 3,
        name: 'Luke Skywalker',
        health: '1234',
        damage: '1234',
        image: 'https://media.giphy.com/media/Qx90DIQmcJlte/giphy.gif'
    }, {
        id: 4,
        name: 'Chewbacca',
        health: '1234',
        damage: '1234',
        image: 'https://media.giphy.com/media/RUUdVZqwpfTRS/giphy.gif'
    }
];

export class ArmyList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayedHeroes : HEROES
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        let searchQuery = event.target.value.toLowerCase();
        let displayedHeroes = HEROES.filter(function (el) {
            let searchValue = el.name.toLowerCase();
            return searchValue.indexOf(searchQuery) !== -1;
        });
        this.setState({
            displayedHeroes: displayedHeroes
        });
    }

    render() {
        return (
            <div id="army">
                <div className="heroes">
                    <input type="text" placeholder="Search..." className="search-field" onChange={this.handleChange}/>
                    <ul className="army-list">
                        {
                            this.state.displayedHeroes.map(function (el) {
                                return <ArmyHero
                                    key={el.id}
                                    name={el.name}
                                    health={el.health}
                                    damage={el.damage}
                                    image={el.image}
                                />;
                            })
                        }
                    </ul>
                </div>
            </div>
        );
    }
}