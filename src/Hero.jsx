import React from "react";

export class Hero extends React.Component {
    render() {
        return (
            <li className="hero">
                <img className="hero-image" src={this.props.image} width="143px" height="143px"/>
            </li>
        );
    }
}