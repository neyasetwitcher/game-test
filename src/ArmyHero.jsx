import React from "react";

export class ArmyHero extends React.Component {
    render() {
        return (
            <li className="hero">
                <img className="hero-image" src={this.props.image} alt="test" width="60px" height="60px"/>
                <div className="hero-info">
                    <div className="hero-name"> {this.props.name} </div>
                    <div className="hero-health">Health: {this.props.health} </div>
                    <div className="hero-damage">Damage: {this.props.damage} </div>
                </div>
            </li>
        );
    }
}