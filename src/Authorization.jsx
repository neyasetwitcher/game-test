import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect,
    withRouter
} from 'react-router-dom';
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';


const fakeAuth = {
    isAuthenticated: false,
    authenticate(cb) {
        this.isAuthenticated = true;
        setTimeout(cb, 100)
    },
    signout(cb) {
        this.isAuthenticated = false;
        setTimeout(cb, 100)
    }
};

export const AuthButton = withRouter(({history}) => (
    fakeAuth.isAuthenticated ? (
        <p>
            Welcome! <button onClick={() => {
            fakeAuth.signout(() => history.push('/'))
        }}>Sign out</button>
        </p>
    ) : (
        <p>You are not logged in.</p>
    )
));

export const PrivateRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            fakeAuth.isAuthenticated ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: "/",
                        state: {from: props.location}
                    }}
                />
            )
        }
    />
);

export class Authorization extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            password: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleRegistration = this.handleRegistration.bind(this.handleRegistration);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleLogin(event) {
        const form = {
            name: this.state.password,
            email: this.state.email
        };

        {/* -----------you would send data to API to get results, I used database for ease, this also clears the form on submit----------------*/
        }

        let response = true;

        if (response) {
            fakeAuth.authenticate(() => {
                this.setState({redirectToReferrer: true});
            });
        } else {
            alert('Sorry!');

        }

        this.setState({
            name: '',
            email: ''
        });
        event.preventDefault();
    }

    handleRegistration(event) {
        const form = {
            name: this.state.password,
            email: this.state.email
        };

        {/* -----------you would send data to API to get results, I used database for ease, this also clears the form on submit----------------*/
        }

        let response = true;

        if (response) {
            fakeAuth.authenticate(() => {
                this.setState({redirectToReferrer: true});
            });
        } else {
            alert('Sorry!');

        }

        this.setState({
            name: '',
            email: ''
        });
        event.preventDefault();
    }

    render() {
        const {from} = this.props.location.state || {from: {pathname: "/army"}};
        const {redirectToReferrer} = this.state;

        if (redirectToReferrer) {
            return <Redirect to={from}/>;
        }

        return (
            <div className="container">
                <div className="row">
                    <Tabs className="tab">
                        <TabList className="nav nav-tabs">
                            <Tab>Sign in</Tab>
                            <Tab>Sign up</Tab>
                        </TabList>
                        <TabPanel className="tab-content tabs">
                            <div className="tab-pane fade in active">
                                <form className="form-horizontal" onSubmit={this.handleLogin}>
                                    <div className="form-group">
                                        <label htmlFor="username">username</label>
                                        <input type="text" name="email" value={this.state.email}
                                               onChange={this.handleChange} className="form-control"/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="passwordLogin">Password</label>
                                        <input type="password" value={this.state.password} name="password"
                                               onChange={this.handleChange} className="form-control"/>
                                    </div>
                                    <div className="form-group">
                                        <button className="btn btn-default" type="submit">Sign in</button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div role="tabpanel" className="tab-pane fade">
                                <form method="post" className="form-horizontal" onSubmit={this.handleRegistration}>
                                    <div className="form-group">
                                        <label htmlFor="firstName">First Name</label>
                                        <input className="form-control" name="firstName" value={this.state.firstName}
                                               onChange={this.handleChange} id="firstName"/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="lastName">Last Name</label>
                                        <input className="form-control" id="lastName" name="lastName"
                                               value={this.state.lastName}
                                               onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="email">Email address</label>
                                        <input type="email" className="form-control" id="email" name="email"
                                               value={this.state.email}
                                               onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="password">Password</label>
                                        <input type="password" className="form-control" id="password" name="password"
                                               value={this.state.password}
                                               onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                        <button className="btn btn-default">Sign up</button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>
                    </Tabs>
                </div>
            </div>
        );
    }
}