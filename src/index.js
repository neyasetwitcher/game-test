import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Authorization, PrivateRoute, AuthButton} from './Authorization.jsx';
import 'bootstrap';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';
import {ArmyList} from "./Army";
import {HeroesList} from "./Fight";


class Page extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <AuthButton />
                    <PrivateRoute path="/fight" component={HeroesList}/>
                    <Route exact path="/" component={Authorization}/>
                    <PrivateRoute path='/army' component={ArmyList}/>
                </div>
            </Router>
        );
    }
}

ReactDOM.render(
    <Page/>,
    document.getElementById("fight")
);
